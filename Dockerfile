FROM alessandroumek/php-apache-node

# install the PHP extensions we need
RUN apt-get update && apt-get install -y wget libpng12-dev libjpeg-dev libpq-dev \
	&& rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-install gd mbstring pdo pdo_mysql pdo_pgsql zip

#INSTALL COMPOSER
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer 

# Install Drush using Composer
RUN composer global require drush/drush:"7.*" --prefer-dist

RUN ln -sf /root/.composer/vendor/bin/drush.php /usr/local/bin/drush && drush status

# GET DRUPAL 7
RUN cd /var/www/ && cd /var/www/ && drush dl --drupal-project-rename=drupal drupal-7.x && chown -R www-data:www-data drupal

# SET Apache document root on drupal
ADD 000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2ensite 000-default.conf

WORKDIR /var/www/drupal

## DOWNLOAD Italian transtaltion
RUN cd profiles/minimal/translations \
	&& wget http://ftp.drupal.org/files/translations/7.x/drupal/drupal-7.41.it.po \
	&& chown -R www-data:www-data ./*
	
